<?php
class themesCompetences{

  private $_nomThemesCompetences;
  public $_tableauObjetTitreCompetences = array();//un tableau d objet titreCompetences
  public $_cmptTitre;
  public $_tailleTableauTitre;

  public function __construct($nomThemesCompetences, $fichierThemesCompetences){
    //echo('On passe le construct themesCompetences </ br>');
    $this->setNomThemesCompetences($nomThemesCompetences);
    //$recupFichier = explode(":", $fichierThemesCompetences);
    $monfichier = fopen($fichierThemesCompetences, 'r+');
    if ($monfichier)
      {
	       /*Tant que l'on est pas à la fin du fichier*/
	        while (!feof($monfichier))
          //foreach ($monFichier as  $lineContent)
	         {
		           /*On lit la ligne courante*/
		             $buffer = fgets($monfichier);
		             /*On l'affiche*/
                 //echo '</ br> LE buffer vaut : ';
		             //echo $buffer;
                 //echo '</ br>';
                 if($buffer == ''){
                   //echo 'Y A RIEN DE LE BUFFER';
                 }else{
                   $recupLigne = explode(":",$buffer);
                   $objetTitreCompetences =  new titreCompetences($recupLigne[0],$recupLigne[1]);
                   $this->_tableauObjetTitreCompetences[] = $objetTitreCompetences;
                   //echo($_tableauObjetTitreCompetences);
                   //var_dump($_tableauObjetTitreCompetences);
                   //echo '</ br> on a du créer l objet</ br>';
                 }
	           }
	         /*On ferme le fichier*/
	         fclose($monfichier);
      }
  }

  public function setNomThemesCompetences($nomThemesCompetences){
    $this->_nomThemesCompetences = $nomThemesCompetences;

  }

  public function getNomThemesCompetences(){
    echo $this->_nomThemesCompetences;

  }

//peu mettre une valeur en parametre pour choisir le titre
//et créer afficher titre init qui initie a valeur à zero
// et suivant precedent joue avec ce numero et en fonction de la taille du tableauCompetences
  public function initAfficherTitre(){
    $this->_cmptTitre = 0;
    $this->_tailleTableauTitre = count($this->_tableauObjetTitreCompetences);
    echo 'compteur vaut : '.$this->_cmptTitre.' </br>';
    echo 'taileTableau vaut : '.$this->_tailleTableauTitre.' </br>';

    //echo '<img src="img/precedent.jpg" alt="filtre" class="col-xs-3" id="precedentTitre" />';
    //echo '<p class="col-xs-6" id="titreCompetences">';
    $this->afficherTitre($this->_cmptTitre);
    //echo '</p>';
    //echo '<img src="img/suivant.jpg" alt="filtre" class="col-xs-3" id="suivantTitre"/>';

  }

  public function afficherTitre($numero){
    //echo 'ON affiche le TITRE ::';

    $this->_tableauObjetTitreCompetences[$numero]->getNomTitreCompetences();

    //echo ':: FINI ::';
  }

  public function suivantAfficherTitre(){
    //echo("cmptTitre  ".$this->_cmptTitre."  tailleTableau ".$this->_tailleTableauTitre);
    if($this->_cmptTitre < $this->_tailleTableauTitre){
      $id = $this->_cmptTitre ;// = $this->_cmptTitre +1;
      $id = $id +1;
      $this->_cmptTitre = $id;

      echo 'id vaut : '.$id.'</br>';
      echo 'compteur vaut : '.$this->_cmptTitre.' </br>';
      echo 'taileTableau vaut : '.$this->_tailleTableauTitre.' </br>';
      $this->afficherTitre($this->_cmptTitre);
      

    }/*else if($this->_cmptTitre == $this->_tailleTableauTitre ){
      $this->_cmptTitre = 0;
      $this->afficherTitre($this->_cmptTitre);
    }*/
  }

  public function precedentAfficherTitre(){
    if($this->_cmptTitre > 0){
      $this->_cmptTitre = $this->_cmptTitre -1;
      echo 'compteur vaut : '.$this->_cmptTitre.' </br>';
      echo 'taileTableau vaut : '.$this->_tailleTableauTitre.' </br>';
      $this->afficherTitre($this->_cmptTitre);


    }/*else if($this->_cmptTitre == 0){
      $this->_cmptTitre = $this->_tailleTableauTitre;
      $this->afficherTitre($this->_cmptTitre);
    }*/
  }
}


class titreCompetences{

  public $_nomTitreCompetences;
  public $_tableauCompetences = array();

  public function __construct($nomTitreCompetences,$recupCompetences){
      //echo'<br/> LE NOM DE LA COMPETENCES EST :'. $nomTitreCompetences.'</ br>';
      $this->setNomTitreCompetences($nomTitreCompetences);
      $donnee = explode("-",$recupCompetences);
      foreach ($donnee as  $value) {
        # code...
        $this->_tableauCompetences[] = new Competences($value);
      }


  }

  public function setNomTitreCompetences($nomTitreCompetences){
    $this->_nomTitreCompetences = $nomTitreCompetences;
  }

  public function getNomTitreCompetences(){
    echo $this->_nomTitreCompetences;
  }

  public function getNomCompetences($i){
      $this->_tableauCompetences[$i]->getNomCompetence();
  }

  public function afficheCompetences(){
    echo '<p>'.$this->getNomTitreCompetences().'</p>';
    echo '<table class="table table-bordered table-striped table-condensed">';
    $taille = count($this->_tableauCompetences);

    for ($i=0; $i<$taille-1; $i++){
    //for ($i=0; $i<3; $i++){
      //echo "<tr><td><div>".$this->_tableauCompetences[$i]."</div></td></tr>";
      echo '<tr><td><div>';
      //echo 'toto';
      $this->getNomCompetences($i);
      echo '</div></td></tr>';
    }
    echo '</table>';
    //return $string;
  }
}

  class Competences{
    public $_nomCompetences ;
    private $_tailleTableau; //= count($_tableauCompetences);

    public function __construct($nomCompetences){
      $this->_nomCompetences = $nomCompetences;
      //echo '</br> la competences 1 est '.$_tableauCompetences[0];
      //var_dump($_tableauCompetences);

    }

    public function getNomCompetence(){
      echo $this->_nomCompetences;
    }


  }

 ?>
