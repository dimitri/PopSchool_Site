<?php
//inclure le fichier connexion.base.php pour pouvoir se connecter à la base de données
include('connexionBase.php');
session_start();
$log = false;

//en cas de récéption d'une erreur 'log' lors de la connexion.
if(isset($_GET['erreur'])){
    $erreur = $_GET['erreur'];
    if($erreur == "log"){
        $log = true;
        $msg = "<div class='erreur'>Vous n'êtes pas connecté veuillez vous connecter.</div>";
    }
}
?>


<?php

//en cas de connexion d'un administrateur on affiche son nom et une lien vers son espace perso
if(isset($_SESSION['adminname'])){
?>
    <div class="message">
        <p class="titre1"><strong>Bienvenue administrateur <?php echo $_SESSION['adminname']; ?></strong></p>
        <div class="monespace"><a href="admin/index.php">Espace Administrateur</a></div><br />
        <div class="logout"><a href="logout.php">Déconnexion</a></div>
    </div>
<?php
}
//en cas de connexion d'un utilisateur on affiche son nom et une lien vers son espace perso
elseif(isset($_SESSION['pseudo'])){
?>
<div class="message">
    <p class="titre1"><strong>Bienvenue <?php echo $_SESSION['pseudo']; ?></strong></p>
    <div class="monespace"><a class="monespace" href="appli.php">Espace utilisateur</a></div><br/>
    <div class="logout"><a class="logout" href="logout.php">Déconnexion</a></div>
</div>
<?php
}
else {
            $ologin = '';
            //On verifie si le formulaire a ete envoye
            if(isset($_POST['pseudo'], $_POST['passwd']))
            {
                    //On echappe les variables pour pouvoir les mettre dans des requetes SQL
                    if(get_magic_quotes_gpc())
                    {
                        $ologin = stripslashes($_POST['pseudo']);
                        $login = mysqli_real_escape_string($mysqli,stripslashes($_POST['pseudo']));
                        $password = stripslashes(md5($_POST['passwd']));

								//Debug
								/*
								echo 'on passe sur get magic true <br />';
								echo $ologin.'<br />' ;
                        echo $login.'<br />';
                        echo $password.'<br />';*/


                    }
                    else
                    {

                        $login = mysqli_real_escape_string($mysqli,$_POST['pseudo']);
                        $password = md5($_POST['passwd']);

//probléme avec fonction md5 les deux mots passe ne sont pas identique

                        //Debug
                        /*
                        $login = $_POST['username'];
                        $password = md5($_POST['password']);
                        */
                        /*echo 'on ne passe pas sur get magic false <br />';
                        echo $login.'<br />';
                        echo $password.'<br />';*/
                    }
                    //On recupere le mot de passe de lutilisateur
                    $req = mysqli_query($mysqli,"select passwd, id from user where pseudo='$login'");
                    //echo 'req = '.$req['motdepasse'].'<br />';
                    //Y A PAS D ADMIN POUR L INSTANT
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //$req1 = mysqli_query($mysqli,"select motdepasse, id_admin from administrateur where login='$login'");
                    //echo 'req1 = '.$req1.'<br />';
                    $dn = mysqli_fetch_array($req);
                    //echo $dn['motdepasse'].'<br />';
                    //Y A PAS D ADMIN POUR L INSTANT
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //$dn1 = mysqli_fetch_array($req1);

                    //echo $dn1.'<br />';
                    //On le compare a celui quil a entre et on verifie si le membre existe
                    if($dn['passwd']==$password and mysqli_num_rows($req)>0)
                    {
                        //Si le mot de passe est bon, on ne vas pas afficher le formulaire
                        $form = false;
                        //On enregistre son pseudo dans la session username et son identifiant dans la session userid
                        //echo 'user = '.$_POST['username'].' et id = '.$dn['id_user'].'<br />';
                        $_SESSION['pseudo'] = $_POST['pseudo'];
                        $_SESSION['id'] = $dn['id'];
                        $id=$dn['id'];
                        //echo 'user = '.$_SESSION['username'].' et id = '.$_SESSION['userid'].'<br />';
                        //ecrire datelastconnect
                        $rqt = "UPDATE user SET datelastconnect= date(now()) WHERE id=$id";

                        $dn = mysqli_query($mysqli, $rqt);
                        header('location: appli.php');
                    }
                    else
                    {
                        //Sinon, on indique que la combinaison nest pas bonne
                        $form = true;
                        $message = 'La combinaison que vous avez entr&eacute; n\'est pas bonne.';
                    }
                    /*if($dn1['passwd']==$password and mysqli_num_rows($req1)>0){
                        $form = false;
                        //On enregistre son pseudo dans la session username et son identifiant dans la session adminid
                        //echo 'admin = '.$_POST['username'].' et id = '.$dn['id_admin'];
                        $_SESSION['adminname'] = $_POST['pseudo'];
                        $_SESSION['adminid'] = $dn1['id'];
                        header('location: admin/index.php');
                        //windows.header
                    }
                    else
                    {
                        //Sinon, on indique que la combinaison n'est pas bonne
                        $form = true;
                        $message = 'Nom d\'utilisateur ou Mot De Passe incorrect. ';
                    }*/
            }
            else
            {
                $form = true;
            }
            if($form)
            {
            //On affiche le formulaire
    ?>

        <form action="connexion.php" method="post">
           <!-- <p class="titre"><strong>Identifiez-vous</strong></p> -->
            <?php
                //On affiche un message s'il y a lieu
                if(isset($message))
                {
                    echo '<div class="erreur">'.$message.'</div>';



                }
                if($log){
                    echo $msg;
                }
            ?>
            <div class="center col-lg-12 col-xs-12">
                <p><label for="pseudo">Nom d'utilisateur</label></p>
                <input type="text" name="pseudo" id="pseudo" value="<?php echo htmlentities($ologin, ENT_QUOTES, 'UTF-8'); ?>" /><br />
                <p><label for="passwd">Mot de passe</label></p>
                <input type="password" name="passwd" id="passwd" /><br /><br/>
                <input type="submit" value="Connexion"  id="connexion"/>
            </div>
        </form>
        <div id="sign_in" class="sign_in"><span><a href="inscription.php">S'inscrire</a></span></div>
        <?php
        }
    }
    ?>
