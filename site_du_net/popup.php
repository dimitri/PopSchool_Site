<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Test popup</title>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="jquery-ui.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.2.1.js"></script>
    <script src="jquery-ui.js"></script>
    <link href="style.css" rel="stylesheet">

    <script src="bootstrap/js/bootstrap.min.js"></script>
  </head>
  <body>
    <!--  afficher touts les postit -->
    <?php
    include_once('connexionBase.php');

    $requete = "select * from postit ";
    //on recup :
    //id du postit
    //id user
    //id competence
    //position
    //niveau
    //date
    $rqt = mysqli_query($mysqli,$requete);

    while($dn = mysqli_fetch_assoc($rqt)){

      //récupérer les info user grace à l id user recup
      //récupérer les info compétence grace à l'id compétence récup
      $requeteUser = "select * from user where id=".$dn['idUser']."";
      //echo $requeteUser;
      $rqtUser = mysqli_query($mysqli, $requeteUser);
      $dnUser = mysqli_fetch_assoc($rqtUser);

      $requeteCompetence = "select * from competences where id=".$dn['idCompetences']."";
      //echo $requeteCompetence;
      $rqtCompetence = mysqli_query($mysqli, $requeteCompetence);
      $dnCompetence = mysqli_fetch_assoc($rqtCompetence);

      //construire le ReflectionExtension
      $info = "<div>
                <p>".$dnUser['pseudo']."         ".$dn['date']."</p>
                <p>    ".$dnCompetence['themeCompetence']." </p>
                <p>        ".$dnCompetence['titreCompetence']."</p>
                <p>                ".$dnCompetence['competence']."</p>
              </div>";

      //echo "<p>".$dn['idCompetences']."</p> </ br>";
      echo '<div class=" popup draggable" draggable="true">
      <a rel="popup" id="'.$dn['id'].'"  title=\''.$info.'\'>
      <img src="img/postit.png" alt="postit" id="962" ondragstart="return false;" onmousedown="false false;">
      </a>
        </div>
        <div class="popup" ><a rel="popup" id="'.$dn['id'].'"  title=\''.$info.'\'>
            Le lien du pop up
        </a></div>';

    }
     ?>
     <script>
       $(document).ready(function() {

         $('.popup').on('mouseover', 'a[rel=popup]', function (e) {
            tip = $(this).attr('title');

            $(this).attr('title','');

            $(this).append('<div id="tooltip"><div class="tipHeader"></div><div class="tipBody">'+tip+'</div><div class="tipFooter"></div></div>');

            $('#tooltip').css('top', e.pageY + 10);
            $('#tooltip').css('left', e.pageX + 20);

            $('#tooltip').fadeIn('500');
            $('#tooltip').fadeTo('10',0.8);
         }).on('mousemove', 'a[rel=popup]', function (e) {
           $('#tooltip').css('top', e.pageY + 10);
           $('#tooltip').css('left', e.pageX + 20);
         }).on('mouseout','a[rel=popup]', function (e){
           $(this).attr('title',$('.tipBody').html());
           $(this).children('div#tooltip').remove();
         });
       });
     </script>
     <style>
        #tooltip {
          position:absolute;
          z-index: 9999;
          color: #fff;
          font-size: 10px;
          width: 300px;
          height: 300px;
          //background-image: '/img/postit.png';
          //background-image: url(img/postit.png);
        }

        #tooltip .tipBody {
          font-size: 30px;
          width: 300px;
          height: 300px;
          background-color: #000;
          padding: 5px;
          background-image: url(img/postit.png);
          background-repeat:no-repeat;
          background-size: 300px 300px;
        }
     </style>
  </body>
</html>
