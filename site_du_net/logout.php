<?php
//Création ou restauration de session.
session_start();
//Détruire toutes les variables de la session courante
session_unset();
//Détruire toutes les données associées à la session courante
session_destroy();
//Rederiction vers la page principale
header("Location:index.html");
?>
