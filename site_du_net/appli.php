<!DOCTYPE html>
<!--
Comment gérer mes objet php javascript
drag drop, quelque soucis de dragImg
et drop est ce dans canvas ou div
gérer la taille de l image en drag et de partir de chaque ligne du tableau
Comment gérer les filtres
-->
<?php include_once('connexionBase.php');
session_start();
 ?>
<?php //include_once('transfertFichierBdd.php'); ?>

<html>
  <head>
    <meta charset="utf-8" />
    <title>Tableau de compétences</title>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="jquery-ui.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.2.1.js"></script>
    <script src="jquery-ui.js"></script>
    <link href="style.css" rel="stylesheet">

    <script src="bootstrap/js/bootstrap.min.js"></script>
    <style>



    </style>

  </head>
  <body>
    <div class="container">
      <header class="row">
        <div class="col-xs-2">
          <!-- <p>ici les pictogramme pour filtre et parametre</p> -->
          <a href="filtres.php" id="filtres" ><img src="img/filtre.png" alt="filtre" class="imgMenu" /></a>

          <?php $id= $_SESSION['id'];

          if($id == 1){
            ?>
            <a href="parametre.php" id="parametre" ><img src="img/config.png" alt="config" class="imgMenu" /></a>

          <!-- organise les postit différament, par nom, mit tel quelle, organisé -->
        <?php }
        ?>
        </div>
        <!-- Entre les parametre et connexion, choisir leur type de compétences, Titre Pro DSH, Programmation, -->
        <div class="col-xs-9 ">
          <?php //include('themesCompetences.php');
          ?>
          <div id="infoTheme" class="col-xs-12" >Choisissez un thème <?php echo $_SESSION['pseudo'];?></div>

        </div>

        <div class=" col-xs-1">


            <a href="logout.php" id="deconnexion" >Déconnection</a>
            <a href="popup.php" id="deconnexion" >Pop up</a>
        </div>
      </header>
    </div>
    <div class="row separation"></div>
    <div class="row themesCompetences">
      <div class="col-xs-12">
        <div id="Informatiques" class=" boutonThemesCompetences">Informatique</div>
        <div id="Popschool" class="boutonThemesCompetences">Pop School</div>
      </div>
    </div>
    <div class="row separation"></div>

    <div class= "row">
      <div class="col-xs-offset-1 col-xs-8">
        <div id="frameTitreCompetences" >
          <div id="precedentTitre" class="col-xs-1 class="nondraggable">
            <img src="img/precedent.jpg" alt="filtre"  id="precedentTitreImg"   ondragstart="return false;" onmousedown="false false;"/>
          </div>
          <div  class="col-xs-10" id="titreCompetences" class="nondraggable"></div>
          <div id="suivantTitre" class="col-xs-1">
            <img src="img/suivant.jpg" alt="filtre"  id="suivantTitreImg"  ondragstart="return false;" onmousedown="false false;"/>
          </div>

        </div>

      </div>
    </div>
    <div class="row">
      <div class="col-xs-1">
        <p>Tableau de nom</p>
      </div>
      <div class="col-xs-8">
        <div class="row">
          <div class="col-xs-12 titre ">
            <p>Tableau de compétences</p>
          </div>
          <div class="col-xs-4">
            <p>Acquise</p>
          </div>
          <div class="col-xs-4">
            <p>En cours</p>
          </div>
          <div class="col-xs-4">
            <p>Help !</p>
          </div>
        </div>
      </div>
      <div class="col-xs-3">
        <p class="col-xs-12">Liste compétence precedent suivant</p>
        <!-- <img src="img/precedent.jpg" alt="filtre" class="col-xs-3" />
        <img src="img/suivant.jpg" alt="filtre" class="col-xs-offset-6 col-xs-3"/> -->

        <?php  //$CompetencesInformatique->initAfficherTitre(); ?>

      </div>
    </div>
    <div class="row">
      <div class="col-xs-1">
        <!-- click au nom voir ses compétences-->
        <!--
        <table  class="table table-bordered table-striped table-condensed dropper">
          <tr >
            <td>User 1</td>
          </tr>
          <tr>
            <td>User 2</td>
          </tr>
          <tr>
            <td>User 3</td>
          </tr>
          <tr>
            <td>User 4</td>
          </tr>
          <tr>
            <td>User 5</td>
          </tr>
        </table>
      -->

        <?php
        $reponse = mysqli_query($mysqli,"SELECT pseudo FROM user ");
        $htmlTableau = "<table class=\"table table-bordered table-striped table-condensed\">";

        while( $pseudo = mysqli_fetch_assoc($reponse)){
          $Cmpt = $pseudo['pseudo'];
          //echo $Cmpt."</ br>";
          $htmlTableau .= "<tr>";
          //$htmlTableau .= '<td><div class="draggable"><img src="img/postit.png"  alt="postit"  id="$value" ondragstart="return false;" onmousedown="false false;" /></div></td>';
          $htmlTableau .=  "<td ><div>".$Cmpt."</div></td></tr>";

        }

        $htmlTableau .= "</table>";
        echo $htmlTableau;

        ?>
      </div>
      <div class="col-xs-8" id="grille">
        <!-- chercher comment afficher les posit déjà mis -->
        <!-- voir plustard pour le canvas
        <canvas class="col-xs-12" id="canvas" width="720" height="500" style = "border: 1px solid">
	    <h4>Oops !!! Votre navigateur ne supporte pas cette technologie,Veuillez installer un navigateur plus récent</h4>
	</canvas> -->
        <?php

        for($i = 0 ; $i < 10; $i++){
          echo '<div class="row">';
          for($j=0; $j<=12;$j++){

            if($j < 4)
            echo '<div class="col-xs-1" id="'.$i.'-'.$j.'"> <div class="dropper vert"></div>  </div>';
            if($j > 4 && $j < 9)
            echo '<div class="col-xs-1" id="'.$i.'-'.$j.'"> <div class="dropper jaune"></div>  </div>';
            if($j > 8)
            echo '<div class="col-xs-1" id="'.$i.'-'.$j.'"> <div class="dropper rouge"></div>  </div>';
          }
          echo '</div>';

        }
   ?>
      </div>
      <div class="col-xs-3" id="frameCompetences">
      <!--  <table  class="table table-bordered table-striped table-condensed">
          <tr>
            <td  class="draggable" draggable="true">Compétence 1</td>
          </tr>
          <tr>
            <td  class="draggable" draggable="true">Compétence 2</td>
          </tr>
          <tr>
            <td  class="draggable" draggable="true">Compétence 3</td>
          </tr>
          <tr>
            <td  class="draggable" draggable="true">Compétence 4</td>
          </tr>
          <tr>
            <td  class="draggable" draggable="true">Compétence 5</td>
          </tr>
        </table> -->
      </div>
    </div>
    <div class="row">
      <div class="col-xs-1 pull-right"><a href="requete.php">requete</a></div>
    </div>



    <script>
        /*  var dragImg = new Image();//Il est conseillé de précharger l'image sinon elle risque de ne pas s'afficher pendant le déplacement
                 dragImg.src = '../competences/img/1-1user1.png';
                 //dragImg.width = '40px';
                 //dragImg.height = '40px';
        //document.querySelector('#draggable').draggable = true;
        //draggableElement.addEventListener('dragstart', function(e) {

      //      e.dataTransfer.setData('text/plain', "Ce texte sera transmis à l'élément HTML de réception");

    //});
        document.querySelector('.draggable').addEventListener('dragstart',function(e){
            e.dataTransfer.setData('text/plain', "");
            e.dataTransfer.setDragImage(dragImg, 0, 0);
        });

        document.querySelector('#dropper').addEventListener('dragover', function(e) {

        e.preventDefault(); // Annule l'interdiction de drop

    });

        document.querySelector('#dropper').addEventListener('drop', function(e){
            e.preventDefault();
            alert('Vous avez bien déposé votre élément !');
        }, false);

        // Cet événement détecte n'importe quel drag & drop qui se termine,     autant le mettre sur « document » :
        var dropper = document.querySelector('#dropper');


        dropper.addEventListener('dragenter', function() {

            dropper.style.borderStyle = 'dashed';

        });


        dropper.addEventListener('dragleave', function() {

            dropper.style.borderStyle = 'solid';

        });


        document.addEventListener('dragend', function() {

            //alert("Un Drag & Drop vient de se terminer mais l'événement dragend ne sait pas si c'est un succès ou non.");

        });*/

                </script>
        <script>
        //quand le document est chargée mettre invisible frameTitreCompetences
        $(document).ready(function(){
            $('#frameTitreCompetences').hide('fast');

        })
        </script>

        <script>
        var nomThemeCompetences = null;
        var titre = null;
        $(document).ready(function () {
          //ici filtrer par themes
          var compteurTitre = 0;

          var tailleTableau = 0;

          $('.boutonThemesCompetences').click(function(){
            //var dateChoisie = $('#datepicker').val();
            nomThemeCompetences = $(this).attr('id');
            compteurTitre = 0;
            $('#frameTitreCompetences').show('fast');
            $("#precedentTitreImg").hide('fast');
            $("#suivantTitreImg").show('fast');

          /*  console.log(dateChoisie);
            $.ajax({
              url: 'filtresThemes.php',
              type: 'get',
              data: 'theme='+nomThemeCompetences+'&iduser='+<?php echo $iduser ?>,
              dataType: 'json',
              complete: function(data){
                	//$('#TitreCompetences').html(data);
              }
            })*/
            //19/12/17
            //recup l id de la div titre, la valeur du contenu html de #TitreCompetences
            // var nomTitre =

             $.ajax({
                   url: 'filtresTitre.php',
                   type: 'post',
                   data: 'theme='+nomThemeCompetences+'&titre='+compteurTitre,
                   dataType: 'json',
                   complete: function(data){
                     console.log(data);
                     $('#titreCompetences').html(data.responseText);
                     titre = $('#titreCompetences').html();
                     //afficher la grille en fonction de theme et titre
                     $.ajax({
                       url: 'affichageGrille.php',
                       type: 'post',
                       data: 'titre='+titre+'&theme='+nomThemeCompetences,
                       dataType: 'json',
                       complete: function(data7){
                         $('#grille').html(data7.responseText);
                       }
                     })
                     console.log(titre);
                     $.ajax({
                       url: 'afficheCompetences.php',
                       type: 'post',
                       data: 'titreCompetence='+titre,
                       dataType: 'json',
                       complete: function(data2){
                         console.log(data2);
                         $('#frameCompetences').html(data2.responseText);
///////////////////////////////////////////////
//functDraginit();

/*
(function() {

var dndHandler = {

draggedElement: null, // Propriété pointant vers l'élément en cours de déplacement

applyDragEvents: function(element) {

element.draggable = true;

var dndHandler = this; // Cette variable est nécessaire pour que l'événement « dragstart » ci-dessous accède facilement au namespace « dndHandler »

element.addEventListener('dragstart', function(e) {
dndHandler.draggedElement = e.target; // On sauvegarde l'élément en cours de déplacement
e.dataTransfer.setData('text/plain', ''); // Nécessaire pour Firefox
});

},

applyDropEvents: function(dropper) {

var recupClass = null;

dropper.addEventListener('dragover', function(e) {
e.preventDefault(); // On autorise le drop d'éléments
if(recupClass == null){
recupClass = this.className;
}

console.log(recupClass);
this.className = recupClass+' drop_hover'; // Et on applique le style adéquat à notre zone de drop quand un élément la survole
//this.className = 'dropper drop_hover';
});

dropper.addEventListener('dragleave', function() {
this.className = recupClass; // On revient au style de base lorsque l'élément quitte la zone de drop
});

var dndHandler = this; // Cette variable est nécessaire pour que l'événement « drop » ci-dessous accède facilement au namespace « dndHandler »

var idBlock = null;
dropper.addEventListener('drop', function(e) {

var target = e.target,
draggedElement = dndHandler.draggedElement, // Récupération de l'élément concerné
clonedElement = draggedElement.cloneNode(true); // On créé immédiatement le clone de cet élément


console.log("cherche id du block");
console.log(target.parentNode.id);
idBlock = target.parentNode.id;

while (target.className.indexOf('dropper') == -1) { // Cette boucle permet de remonter jusqu'à la zone de drop parente
target = target.parentNode;
}

target.className = recupClass;//'dropper'; // Application du style par défaut

var enfant = $("#"+idBlock+" .dropper").children().length;
console.log("il a "+enfant+" enfants");
if(enfant >= 2){
  alert("la div contient déjà 2 éléments");
}else{
  clonedElement = target.appendChild(clonedElement); // Ajout de l'élément cloné à la zone de drop actuelle
  dndHandler.applyDragEvents(clonedElement); // Nouvelle application des événements qui ont été perdus lors du cloneNode()

  draggedElement.parentNode.removeChild(draggedElement); // Suppression de l'élément d'origine


  var nVarNom = prompt("quel est votre niveau ? '-' débutant  '+' basique  '*' confirmé  '^' GOD")// quel votre niveau
  console.log("on cherche la position et l id du postit");
  console.log(target.parentNode.id);
  //console.log(target.children.id);
  console.log(target.firstChild.firstChild.id);
  //console.log(target);

  var position = target.parentNode.id;
  var idpostit = target.firstChild.firstChild.id;
  console.log("qua vaut le rsultat du prompt "+nVarNom);
  //envoyer insert ou update dans la basique
  //iduser se recupére par la session


  //trouver une solution car le symbol + ne passe passe
  //
  //switch (nVarNom){
  //  case(="+")
  //}
  //
  $.ajax({
      url: 'writeBddPostit.php',
      type: 'post',
      data: 'idpostit='+idpostit+'&niveau='+nVarNom+'&position='+position,
      dataType: 'html',

      complete: function (data) {
          //$('#journal').html(data.responseText);
          //$('#journal').show('fast');
      }
  })
}
//appel ajax pour inscrire dans la bdd
//param id user - id post it - niveau - position - dataTransfer
//si id postit jamais mis insert
//si existe update
});

}

};

var elements = document.querySelectorAll('.draggable'),
elementsLen = elements.length;

for (var i = 0; i < elementsLen; i++) {
dndHandler.applyDragEvents(elements[i]); // Application des paramètres nécessaires aux éléments déplaçables
}

var droppers = document.querySelectorAll('.dropper'),
droppersLen = droppers.length;

for (var i = 0; i < droppersLen; i++) {
dndHandler.applyDropEvents(droppers[i]); // Application des événements nécessaires aux zones de drop
}

})();
*/


////////////////////::////////////////////////
                       }
                     })
                   }
                 })

                 //init taille tableauTitre
                 $.ajax({
                   url: 'tailleTableau.php',
                   type: 'post',
                   data: 'theme='+nomThemeCompetences,
                   dataType: 'json',
                   complete: function(data3){
                     taillTableau = data3.responseText;
                   }
                 })
                 console.log("la taille du tableau vaut "+tailleTableau);
          })

          $('#suivantTitre').click(function(){
            $("#precedentTitreImg").show('fast');
            console.log(compteurTitre);
            compteurTitre = compteurTitre + 1;
            console.log(compteurTitre);
            $.ajax({
                  url: 'filtresTitre.php',
                  type: 'post',
                  data: 'theme='+nomThemeCompetences+'&titre='+compteurTitre,
                  dataType: 'json',
                  complete: function(data){
                    console.log(data);
                    $('#titreCompetences').html(data.responseText);
                    titre = $('#titreCompetences').html();
                    console.log(titre);
                    //afficher la grille en fonction de theme et titre
                    $.ajax({
                      url: 'affichageGrille.php',
                      type: 'post',
                      data: 'titre='+titre+'&theme='+nomThemeCompetences,
                      dataType: 'json',
                      complete: function(data7){
                        $('#grille').html(data7.responseText);
                      }
                    })
                    $.ajax({
                      url: 'afficheCompetences.php',
                      type: 'post',
                      data: 'titreCompetence='+titre,
                      dataType: 'json',
                      complete: function(data2){
                        console.log(data2);
                        $('#frameCompetences').html(data2.responseText);
                        //functDraginit();
                      }
                    })
                  }
                })

                if(compteurTitre === taillTableau-1){
                  $('#suivantTitreImg').hide('fast');
                }

                //if compteur est à la fin ne pas afficher l Image
          })

          $('#precedentTitre').click(function(){
            $('#suivantTitreImg').show('fast');
            compteurTitre = compteurTitre -1;
            $.ajax({
              url: 'filtresTitre.php',
              type: 'post',
              data: 'theme='+nomThemeCompetences+'&titre='+compteurTitre,
              dataType: 'json',
              complete: function(data4){
                $('#titreCompetences').html(data4.responseText);
                titre = $('#titreCompetences').html();
                //afficher la grille en fonction de theme et titre
                $.ajax({
                  url: 'affichageGrille.php',
                  type: 'post',
                  data: 'titre='+titre+'&theme='+nomThemeCompetences,
                  dataType: 'json',
                  complete: function(data7){
                    $('#grille').html(data7.responseText);
                  }
                })
                $.ajax({
                  url: 'afficheCompetences.php',
                  type:'post',
                  data: 'titreCompetence='+titre,
                  dataType: 'json',
                  complete: function(data5){
                    $('#frameCompetences').html(data5.responseText);
                    //functDraginit();
                  }
                })
              }
            })

            if(compteurTitre === 0){
              $('#precedentTitreImg').hide('fast');
            }
          })



          //créer bouton suivant e précedént
          //déjà incrémenter variable
          //var data;

        })
        </script>
        <script>
        $('#deconnexion').click(function(){
          console.log("on click sur deconnection passage ajax");


            $.ajax({
                url: 'logout.php',
                type: 'post',
                data: '',
                dataType: 'json',
                location: index.php,
                complete: function (data) {
                    //$('#journal').html(data.responseText);
                    //$('#journal').show('fast');
                }
            })

        })
        </script>
        <script>
        //Création de fonction pour le drag and drop
        var nVarNom ="Rien";
        var functDraginit = function() {

        var idpostit = 0;

        var dndHandler = {

        draggedElement: null, // Propriété pointant vers l'élément en cours de déplacement

        applyDragEvents: function(element) {

        element.draggable = true;

        var dndHandler = this; // Cette variable est nécessaire pour que l'événement « dragstart » ci-dessous accède facilement au namespace « dndHandler »

        element.addEventListener('dragstart', function(e) {
        dndHandler.draggedElement = e.target; // On sauvegarde l'élément en cours de déplacement
        e.dataTransfer.setData('text/plain', ''); // Nécessaire pour Firefox
        console.log("---------e.target------------------");
        /*console.log(e.target);
        console.log(e.target.firstChild.id);
        idpostit = e.target.firstChild.id;*/
        console.log(e.target.id);
        idpostit = e.target.id;
        console.log("---------------------------");
        });

        },

        applyDropEvents: function(dropper) {

        var recupClass = null;


        dropper.addEventListener('dragover', function(e) {
        e.preventDefault(); // On autorise le drop d'éléments
        if(recupClass == null){
        recupClass = this.className;
        console.log("---------this dragover------------------");
        console.log(this);
        console.log("---------------------------");

        }

        console.log(recupClass);
        this.className = recupClass+' drop_hover'; // Et on applique le style adéquat à notre zone de drop quand un élément la survole
        //this.className = 'dropper drop_hover';
        });

        dropper.addEventListener('dragleave', function() {
        this.className = recupClass; // On revient au style de base lorsque l'élément quitte la zone de drop
        });

        var dndHandler = this; // Cette variable est nécessaire pour que l'événement « drop » ci-dessous accède facilement au namespace « dndHandler »

        var idBlock = null;
        dropper.addEventListener('drop', function(e) {

        var target = e.target,
        draggedElement = dndHandler.draggedElement, // Récupération de l'élément concerné
        clonedElement = draggedElement.cloneNode(true); // On créé immédiatement le clone de cet élément


        console.log("cherche id du block");
        console.log(target.parentNode.id);
        idBlock = target.parentNode.id;

        while (target.className.indexOf('dropper') == -1) { // Cette boucle permet de remonter jusqu'à la zone de drop parente
        target = target.parentNode;
        }

        target.className = recupClass;//'dropper'; // Application du style par défaut

        var enfant = $("#"+idBlock+" .dropper").children().length;
        console.log("il a "+enfant+" enfants");
        if(enfant >= 3){
          alert("la div contient déjà 3 éléments");
        }else{
          clonedElement = target.appendChild(clonedElement); // Ajout de l'élément cloné à la zone de drop actuelle
          dndHandler.applyDragEvents(clonedElement); // Nouvelle application des événements qui ont été perdus lors du cloneNode()

          draggedElement.parentNode.removeChild(draggedElement); // Suppression de l'élément d'origine


          //si c est pas sa premiere fois ne plus affcher le message
          console.log("----------------------cherche le parent pour l alerte");

          console.log(target.parentNode);

          console.log('----------------------');
          $.ajax({
            url: 'getNiveau.php',
            type: 'post',
            data: 'idpostit='+idpostit,
            dataType: 'json',
            complete: function (data) {
              console.log("-----________________------------on get niveau");
              console.log(data);
              nVarNom = data.responseText;
              console.log(data.responseText);
              console.log("le nom ");
              console.log(nVarNom);
              nVarNom = prompt("quel est votre niveau ? '-' débutant  '+' basique  '*' confirmé  '^' GOD",nVarNom)// quel votre niveau
              console.log("on cherche la position et l id du postit");
              console.log(target.parentNode.id);
              //console.log(target.children.id);
              console.log(target.firstChild.firstChild.id);
              //console.log(target);

              var position = target.parentNode.id;
              //on va cherche l id du posit au debut du dragstart
              //var idpostit = target.firstChild.firstChild.id;
              console.log("que vaut le rsultat du prompt "+nVarNom);
              //envoyer insert ou update dans la basique
              //iduser se recupére par la session

              $.ajax({
                  url: 'writeBddPostit.php',
                  type: 'post',
                  data: 'idpostit='+idpostit+'&niveau='+nVarNom+'&position='+position,
                  dataType: 'html',

                  complete: function (data) {
                      //$('#journal').html(data.responseText);
                      //$('#journal').show('fast');
                      titre= titre.trim();
                      console.log("-----------------------dans le writeBddPostit");
                      console.log("titre="+titre+" theme="+nomThemeCompetences);
                      $.ajax({
                        url: 'affichageGrille.php',
                        type: 'post',
                        data: 'titre='+titre+'&theme='+nomThemeCompetences,
                        dataType: 'json',
                        complete: function(data7){
                          $('#grille').html(data7.responseText);
                          //functDraginit();
                        }
                      })

                  }
              })

            }
          })


          //trouver une solution car le symbol + ne passe passe
          /*
          switch (nVarNom){
            case(="+")
          }
          */

          //titre = $('#titreCompetences').html();

        }
        //appel ajax pour inscrire dans la bdd
        //param id user - id post it - niveau - position - dataTransfer
        //si id postit jamais mis insert
        //si existe update
        });

        }

        };

        var elements = document.querySelectorAll('.draggable'),
        elementsLen = elements.length;

        for (var i = 0; i < elementsLen; i++) {
        dndHandler.applyDragEvents(elements[i]); // Application des paramètres nécessaires aux éléments déplaçables
        }

        var droppers = document.querySelectorAll('.dropper'),
        droppersLen = droppers.length;

        for (var i = 0; i < droppersLen; i++) {
        dndHandler.applyDropEvents(droppers[i]); // Application des événements nécessaires aux zones de drop
        }

        };


        </script>
        <script>
        var functDragmouv = function() {

        var idpostit = 0;

        var dndHandler = {

        draggedElement: null, // Propriété pointant vers l'élément en cours de déplacement

        applyDragEvents: function(element) {

        element.draggable = true;

        var dndHandler = this; // Cette variable est nécessaire pour que l'événement « dragstart » ci-dessous accède facilement au namespace « dndHandler »

        element.addEventListener('dragstart', function(e) {
        dndHandler.draggedElement = e.target; // On sauvegarde l'élément en cours de déplacement
        e.dataTransfer.setData('text/plain', ''); // Nécessaire pour Firefox
        console.log("---------e.target------------------");
        /*console.log(e.target);
        console.log(e.target.firstChild.id);
        idpostit = e.target.firstChild.id;*/
        console.log(e.target.id);
        idpostit = e.target.id;
        console.log("---------------------------");
        });

        },

        applyDropEvents: function(dropper) {

        var recupClass = null;


        dropper.addEventListener('dragover', function(e) {
        e.preventDefault(); // On autorise le drop d'éléments
        if(recupClass == null){
        recupClass = this.className;
        console.log("---------this dragover------------------");
        console.log(this);
        console.log("---------------------------");

        }

        console.log(recupClass);
        this.className = recupClass+' drop_hover'; // Et on applique le style adéquat à notre zone de drop quand un élément la survole
        //this.className = 'dropper drop_hover';
        });

        dropper.addEventListener('dragleave', function() {
        this.className = recupClass; // On revient au style de base lorsque l'élément quitte la zone de drop
        });

        var dndHandler = this; // Cette variable est nécessaire pour que l'événement « drop » ci-dessous accède facilement au namespace « dndHandler »

        var idBlock = null;
        dropper.addEventListener('drop', function(e) {

        var target = e.target,
        draggedElement = dndHandler.draggedElement, // Récupération de l'élément concerné
        clonedElement = draggedElement.cloneNode(true); // On créé immédiatement le clone de cet élément


        console.log("cherche id du block");
        console.log(target.parentNode.id);
        idBlock = target.parentNode.id;

        while (target.className.indexOf('dropper') == -1) { // Cette boucle permet de remonter jusqu'à la zone de drop parente
        target = target.parentNode;
        }

        target.className = recupClass;//'dropper'; // Application du style par défaut

        var enfant = $("#"+idBlock+" .dropper").children().length;
        console.log("il a "+enfant+" enfants");
        if(enfant >= 3){
          alert("la div contient déjà 3 éléments");
        }else{
          clonedElement = target.appendChild(clonedElement); // Ajout de l'élément cloné à la zone de drop actuelle
          dndHandler.applyDragEvents(clonedElement); // Nouvelle application des événements qui ont été perdus lors du cloneNode()

          draggedElement.parentNode.removeChild(draggedElement); // Suppression de l'élément d'origine


          //si c est pas sa premiere fois ne plus affcher le message
          console.log("----------------------cherche le parent pour l alerte");

          console.log(target.parentNode);

          console.log('----------------------');

          var nVarNom = prompt("quel est votre niveau ? '-' débutant  '+' basique  '*' confirmé  '^' GOD",)// quel votre niveau
          console.log("on cherche la position et l id du postit");
          console.log(target.parentNode.id);
          //console.log(target.children.id);
          console.log(target.firstChild.firstChild.id);
          //console.log(target);

          var position = target.parentNode.id;
          //on va cherche l id du posit au debut du dragstart
          //var idpostit = target.firstChild.firstChild.id;
          console.log("que vaut le rsultat du prompt "+nVarNom);
          //envoyer insert ou update dans la basique
          //iduser se recupére par la session


          //trouver une solution car le symbol + ne passe passe
          /*
          switch (nVarNom){
            case(="+")
          }
          */
          $.ajax({
              url: 'writeBddPostit.php',
              type: 'post',
              data: 'idpostit='+idpostit+'&niveau='+nVarNom+'&position='+position,
              dataType: 'html',

              complete: function (data) {
                  //$('#journal').html(data.responseText);
                  //$('#journal').show('fast');


              }
          })
          //titre = $('#titreCompetences').html();
          titre= titre.trim();
          console.log("-----------------------dans le writeBddPostit");
          console.log("titre="+titre+" theme="+nomThemeCompetences);
          $.ajax({
            url: 'affichageGrille.php',
            type: 'post',
            data: 'titre='+titre+'&theme='+nomThemeCompetences,
            dataType: 'json',
            complete: function(data7){
              $('#grille').html(data7.responseText);
              functDragmouv();
            }
          })
        }
        //appel ajax pour inscrire dans la bdd
        //param id user - id post it - niveau - position - dataTransfer
        //si id postit jamais mis insert
        //si existe update
        });

        }

        };

        var elements = document.querySelectorAll('.draggable'),
        elementsLen = elements.length;

        for (var i = 0; i < elementsLen; i++) {
        dndHandler.applyDragEvents(elements[i]); // Application des paramètres nécessaires aux éléments déplaçables
        }

        var droppers = document.querySelectorAll('.dropper'),
        droppersLen = droppers.length;

        for (var i = 0; i < droppersLen; i++) {
        dndHandler.applyDropEvents(droppers[i]); // Application des événements nécessaires aux zones de drop
        }

        };


        </script>




  </body>
</html>
