<!-- Mettre ici de quoi rajouter des compétences et voir les ensembles -->
<!DOCTYPE html>
<html>
  <head>
    <metacharset="utf_8" />
    <title>Test popup</title>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="jquery-ui.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.2.1.js"></script>
    <script src="jquery-ui.js"></script>
    <link href="style.css" rel="stylesheet">

    <script src="bootstrap/js/bootstrap.min.js"></script>
  </head>
  <body>
    <!--  afficher touts les postit -->
    <?php
    //afficher les thémes en Haut
    //suivant le themes afficher les tableaux correpondant

    //prévoir 2 mise en page suivant les 2 thémes (un horizontal ==  POP,  un vertical || Info )

    include_once('connexionBase.php');

    $requete = "select distinct themeCompetence from competences ";
    $rqt = mysqli_query($mysqli, $requete);

    ?>
    <div class="container">
      <div class="row">
        <?php
          while($dn = mysqli_fetch_assoc($rqt)){
            echo '<div class="theme col-xs-2" id="'.$dn['themeCompetence'].'">'.$dn['themeCompetence'].'</div>';
          }
        ?>
        <a class="pull-right" href="appli.php" >Appli</a>
      </div>
      <div class="tableaux "></div>
    </div>

  <script>
  $(document).ready(function() {
    var nomTheme;
    $('.theme').click(function(){
      nomTheme = $(this).attr('id');
      $.ajax({
        url: 'afficheTableauParam.php',
        type: 'post',
        data: 'theme='+nomTheme,
        dataType: 'json',
        complete: function(data){
          $('.tableaux').html(data.responseText);
//créer les click des td, qui affiche les infos
          $('td').click(function() {
            competence = $(this).html();
            console.log(competence);
            id = $(this).attr('id');
            console.log(id);

            $.ajax({
              url: 'infoCompetence.php',
              type: 'get',
              data: 'cmpt='+id,
              dataType: 'json',
              complete: function(data){
                $('.tableaux').html(data.responseText);
              }
            })
          })

        }
      })
    })
  })
  </script>
  </body>
</html>
